package com.dellamarda.searchmemo.utils;

public class Constant {

    public static final String ID_USER = "id_user";
    public static final String KEY_TOKEN = "key_token";
    public static final String USER_KEY = "user_key";

}
